<a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
    <!--img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo2.png" alt="" /-->
    Gesso Ingleses
</a>

<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu( array(
               'menu'              => 'primary_navigation',
               'theme_location'    => 'primary_navigation',
               'depth'             => 2,
               'container'         => 'nav',
               'container_class'   => 'collapse navbar-collapse',
       'container_id'      => 'navbar-collapse',
               'menu_class'        => 'nav navbar-nav',
               'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
               'walker'            => new wp_bootstrap_navwalker())
           );
      endif;
      ?>

  </div>
</header>
