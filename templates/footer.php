<footer class="content-info" role="contentinfo">
  <div class="container">
      <div class="row">
          <div class="col-md-4">
               <p class="footer-titulo">Gesso Ingleses</p>
          </div>
          <div class="col-md-4">
              <p class="footer-centro">
                  <span class="glyphicon glyphicon-earphone" aria-hidden="true" style="font-size:20px;"></span><br />
                  (48) 8808-8697 / (48) 8808-8697 / (48) 8808-8697
              </p>
          </div>
          <div class="col-md-4">
               <p class="footer-contato">
                   <span class="glyphicon glyphicon-envelope" aria-hidden="true" style="font-size:20px;"></span><br />
                   gessoingleses_adonias@hotmail.com
               </p>
          </div>
      </div>


  </div>
</footer>
