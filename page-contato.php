<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/page', 'header'); ?>
    <div class="row">
        <div class="col-md-5">
            <?php echo do_shortcode('[contact-form-7 id="17" title="Formulário de contato 1"]'); ?>
        </div>
        <div class="col-md-7">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1037.538500729201!2d-48.4112895135422!3d-27.49483520638341!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1442943742762" width="100%" height="220" frameborder="0" style="border:0" allowfullscreen></iframe>
            <h4>Gesso Ingleses</h4>
            <h5>Rua Alfa, nº 1890 - Rio Vermelho</h5>
            <h5>48 8808-8697 / 9643-1738 / 84648620</h5>
            <h5>gessoingleses_adonias@hotmail.com</h5>
        </div>
    </div>
<?php endwhile; ?>
