<div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="http://www.gessolizo.com.br/files/gessolizo_gesso3.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Forro de Gesso decorado</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="second-slide" src="http://www.gessolizo.com.br/files/gessolizo_gesso3.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Forro de Gesso Acústico</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="third-slide" src="http://www.gessolizo.com.br/files/gessolizo_gesso3.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Forro de Gesso Trabalhado</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

    <div class="row">

        <div class="col-sm-4">
          <div class="marketing">
          <div class="img-mkt">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon1.png">
              </div
          </div>
          <h2>Gesso</h2>
          <p>
            Trabalhamos com gesso decorado, liso, acústico, emoldurado, trabalhado, gesso com iluminação indireta e divisória.
          </p>
          <a class="btn btn-default" href="#" role="button">Ver Mais &raquo;</a>
        </div>
        </div><!-- /.col-lg-4 -->
        <div class="col-sm-4">
          <div class="marketing">
            <div class="img-mkt">
          <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon2.png">
        </div>
          <h2>Obras</h2>
          <p>
            Já realizamos inúmeros projetos residencias e multi-familiares, conheça algumas obras que já executamos nosssos serviços.
          </p>
          <a class="btn btn-default" href="#" role="button">Ver Mais &raquo;</a>
        </div>
        </div><!-- /.col-lg-4 -->
        <div class="col-sm-4">
          <div class="marketing">
            <div class="img-mkt">
          <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon3.png">
        </div>
          <h2>Contato</h2>
          <p>
            Entre em contato conosco, faça um orçamento, teremos o prazer em lhe atender.
          </p>
          <a class="btn btn-default" href="#" role="button">Ver Mais &raquo;</a>
        </div><!-- /.col-lg-4 -->
      </div>
      </div>
      </div>
