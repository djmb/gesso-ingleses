<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>

<div class="obras">
  <div class="row">
    <div class="col-md-4 col-sm-6">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/terreno.jpg" alt="" class="img-responsive"/>
    </div>
    <div class="col-md-4 col-sm-6">
      <div class="row">
        <div class="col-sm-6 hidden-xs">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/terreno.jpg" alt="" class="img-responsive"/>
        </div>
        <div class="col-sm-6 hidden-xs">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/terreno.jpg" alt="" class="img-responsive"/>
        </div>
        <div class="col-sm-6 hidden-xs">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/terreno.jpg" alt="" class="img-responsive"/>
        </div>
        <div class="col-sm-6 col-xs-12">
          <div class="mais-fotos">
            <p>
              Mais Fotos
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
<h4>Residencial Trier</h4>
<h5>Edificio Multi Familiar</h5>
      <p>Realização da intalação de forro de gesso liso nas áreas comuns e de gesso decorado no salão de festa e espaço gourmet.</p>
    </div>
  </div>
</div>
<div class="obras">
  <div class="row">
    <div class="col-md-4 col-sm-6">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/terreno.jpg" alt="" class="img-responsive"/>
    </div>
    <div class="col-md-4 col-sm-6">
      <div class="row">
        <div class="col-sm-6 hidden-xs">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/terreno.jpg" alt="" class="img-responsive"/>
        </div>
        <div class="col-sm-6 hidden-xs">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/terreno.jpg" alt="" class="img-responsive"/>
        </div>
        <div class="col-sm-6 hidden-xs">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/terreno.jpg" alt="" class="img-responsive"/>
        </div>
        <div class="col-sm-6 col-xs-12">
          <div class="mais-fotos">
            <p>
              Mais Fotos
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <h4>Casa Alfa</h4>
      <h5>Residencia</h5>
      <p>
        At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
      </p>
    </div>
  </div>
</div>
<?php endwhile; ?>
